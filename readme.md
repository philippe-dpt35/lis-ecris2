# Je lis puis j'écris des phrases #

Adapation de l'application éducative PragmaTICE "Je lis puis j'écris" de la compilation d'applications Clicmenu.

L'application originale a été adaptée en deux applications, "Je lis puis j'écris des mots" (lis-ecris), "Je lis puis j'écris des phrases" (lis-ecris2).

L'activité consiste à réécrire des modèles de phrases.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/lis-ecris2/index.html)
