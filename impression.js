function construitPageImpression() {
   let laDate = new Date();
  let liste = "";
  let zoneDate = document.getElementById('date');
  let Nom = document.getElementById('nom');
  let nbExos = document.getElementById('nbexercices');
  let listeExos = document.getElementById('liste-exercices');
  let TotalEpreuve = document.getElementById('nbtotalepreuves');
  let Taux = document.getElementById('tauxexos');
  let Echecs = document.getElementById('nbtotalechecs');

  // Récupère et affiche les données stockées localement
  zoneDate.innerHTML = "Date : " + laDate.getDate() +"/" + (laDate.getMonth()+1) +"/" + laDate.getFullYear();
  Nom.innerHTML = sessionStorage.getItem("nom");
  nbExos.innerHTML = sessionStorage.getItem("exos");
  tabExos = sessionStorage.getItem("liste-exos");
  liste = sessionStorage.getItem("liste-exos");
  listeExos.innerHTML = liste.replace(",", " - ");
  TotalEpreuve.innerHTML = sessionStorage.getItem("epreuves");
  Taux.innerHTML = sessionStorage.getItem("taux") + "%";
  Echecs.innerHTML = sessionStorage.getItem("echecs");

  // Supprime les données stockées localement
  sessionStorage.removeItem("nom");
  sessionStorage.removeItem("exos");
  sessionStorage.removeItem("liste-exos");
  sessionStorage.removeItem("epreuves");
  sessionStorage.removeItem("taux");
  sessionStorage.removeItem("echecs");

}
window.onload = construitPageImpression();